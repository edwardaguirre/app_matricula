var express = require('express');
var router = express.Router();
var model = require('../models/index');

/* GET Usuarios listing. */


/* POST Usuario. */
router.get('/create/:nombre/:apellido/:dni', function (req, res, next) {

    model.Usuario.create({
        nombre: req.params.nombre,
        apellido: req.params.apellido,
        dni: req.params.dni,
        pin: 123456

    }).then(Usuario => {

        res.status(201).json({
            error: false,
            data: Usuario,
            message: 'Nuevo usuario creado.'
        })

    }).catch(error => res.json({
        data: [],
        error: true
    }));

});

router.get('/ver-detalle/:dni_usuario', function (req, res, next) {
    model.sequelize.query(`SELECT * FROM usuario WHERE usuario.dni = ${req.params.dni_usuario} limit 1`, { type: model.sequelize.QueryTypes.SELECT })
        .then(Matricula => res.json({
            error: false,
            data: Matricula
        }))
        .catch(error => res.json({
            data: [],
            error: true
        }));
});


module.exports = router;