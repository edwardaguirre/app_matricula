var express = require('express');
var router = express.Router();
var model = require('../models/index');

/* GET categories listing. */
router.get('/list-estudiantes-matriculados', function (req, res, next) {
    model.sequelize.query('SELECT matricula.id AS id_matricula, matricula.*, carrera.*, usuario.* FROM matricula JOIN usuario on matricula.dni_usuario = usuario.dni JOIN carrera on matricula.id_carrera = carrera.id WHERE matricula.estado = "aceptado"', { type: model.sequelize.QueryTypes.SELECT })
        .then(Matricula => res.json({
            error: false,
            data: Matricula
        }))
        .catch(error => res.json({
            data: [],
            error: true
        }));
});
router.get('/list-estudiantes-pendientes', function (req, res, next) {
    model.sequelize.query('SELECT matricula.id AS id_matricula, matricula.*, carrera.*, usuario.* FROM matricula JOIN usuario on matricula.dni_usuario = usuario.dni JOIN carrera on matricula.id_carrera = carrera.id WHERE matricula.estado = "pendiente"', { type: model.sequelize.QueryTypes.SELECT })
        .then(Matricula => res.json({
            error: false,
            data: Matricula
        }))
        .catch(error => res.json({
            data: [],
            error: true
        }));
});
router.get('/ver-detalle/:dni_usuario', function (req, res, next) {
    model.sequelize.query(`SELECT matricula.id AS id_matricula, matricula.*, carrera.*, usuario.* FROM matricula JOIN usuario on matricula.dni_usuario = usuario.dni JOIN carrera on matricula.id_carrera = carrera.id WHERE usuario.dni = ${req.params.dni_usuario}`, { type: model.sequelize.QueryTypes.SELECT })
        .then(Matricula => res.json({
            error: false,
            data: Matricula
        }))
        .catch(error => res.json({
            data: [],
            error: true
        }));
});


router.get('/aceptar-matricula/:id_matricula', function (req, res, next) {
    model.sequelize.query(`UPDATE matricula SET matricula.estado = "aceptado" WHERE matricula.id = ${req.params.id_matricula}`, { type: model.sequelize.QueryTypes.SELECT })
        .then(Matricula => res.json({
            error: false,
            data: Matricula
        }))
        .catch(error => res.json({
            data: [],
            error: true
        }));
});

router.get('/create/:json', function (req, res, next) {

    let buff = new Buffer(req.params.json, 'base64');
    let obj = JSON.parse(buff.toString('ascii'));

    let allAsignature = [];

    const {
        student: { dni }, cSelected, id_carrera
    } = obj;

    // for (let index = 0; index < cSelected.length; index++) {
    //     let asignature = {

    //     };

    // }


    model.Matricula.create({
        estado: 'pendiente',
        dni_usuario: dni,
        id_carrera: id_carrera,
        fecha_matricula: "2019-09-09"

    }).then(Matricula =>
        res.status(201).json({
            error: false,
            data: Matricula,
            message: 'La matricula se ha realizado correctamente, queda pendiente de aceptación.'
        })
    ).catch(error => res.json({
        data: [],
        error: true
    }));


});
/* update Matricula. */
router.put('/:id', function (req, res, next) {
    const AsignaturaId = req.params.id;

    const { nombre_asignatura } = req.body;

    model.Matricula.update({
        nombre_asignatura: nombre_asignatura
    }, {
        where: {
            id: AsignaturaId
        }
    })
        .then(Matricula => res.json({
            error: false,
            message: 'Se ha actualizado correctamente.'
        }))
        .catch(error => res.json({
            error: true,
        }));
});
/* delete Matricula. */
router.delete('/:id', function (req, res, next) {
    const AsignaturaId = req.params.id;

    model.Matricula.destroy({
        where: {
            id: AsignaturaId
        }
    })
        .then(status => res.json({
            error: false,
            message: 'Se ha eliminado correctamente.'
        }))
        .catch(error => res.json({
            error: error
        }));
});

module.exports = router;