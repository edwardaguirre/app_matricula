'use strict';
module.exports = (sequelize, DataTypes) => {
  const Matricula = sequelize.define('Matricula', {
    fecha_matricula: DataTypes.DATE,
    estado: DataTypes.STRING,
    dni_usuario: DataTypes.INTEGER,
    id_carrera: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'matricula'
  });
  Matricula.associate = function (models) {
    // associations can be defined here
  };
  return Matricula;
};