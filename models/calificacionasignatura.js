'use strict';
module.exports = (sequelize, DataTypes) => {
  const CalificacionAsignatura = sequelize.define('CalificacionAsignatura', {
    puntage: DataTypes.INTEGER,
    periodo: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'calificacion_asignatura'
  });
  CalificacionAsignatura.associate = function (models) {
    // associations can be defined here
  };
  return CalificacionAsignatura;
};