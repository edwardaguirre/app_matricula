'use strict';
module.exports = (sequelize, DataTypes) => {
  const Carrera = sequelize.define('Carrera', {
    nombre_carrera: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'carrera'
  });
  Carrera.associate = function (models) {
    // associations can be defined here
  };
  return Carrera;
};