'use strict';
module.exports = (sequelize, DataTypes) => {
  const Usuario = sequelize.define('Usuario', {
    nombre: DataTypes.STRING,
    apellido: DataTypes.STRING,
    dni: DataTypes.INTEGER,
    pin: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'usuario'
  });
  Usuario.associate = function (models) {
    // associations can be defined here
  };
  return Usuario;
};