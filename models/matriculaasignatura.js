'use strict';
module.exports = (sequelize, DataTypes) => {
  const MatriculaAsignatura = sequelize.define('MatriculaAsignatura', {
    id_matricula: DataTypes.INTEGER,
    id_asignatura: DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'matricula_asignatura'
  });
  MatriculaAsignatura.associate = function (models) {
    // associations can be defined here
  };
  return MatriculaAsignatura;
};