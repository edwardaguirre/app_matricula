'use strict';
module.exports = (sequelize, DataTypes) => {
  const Asignatura = sequelize.define('Asignatura', {
    nombre_asignatura: DataTypes.STRING
  }, {
    freezeTableName: true,
    tableName: 'asignadura'
  });
  Asignatura.associate = function (models) {
    // associations can be defined here
  };
  return Asignatura;
};